import styled from "@emotion/styled";
import { Box, Flex } from "rebass/styled-components";

declare module "react" {
  interface HTMLAttributes<T> extends DOMAttributes<T> {
    active?: boolean;
  }
}

export const Nav = styled("div")`
  & > * {
    margin-left: 1em;
    color: white;
  }
  background: black;
  padding: 1em;
  height: 2em;
  display: flex;
  align-items: center;
`;
export const PageBody = styled("div")`
  width: 30em;
  height: 100%;
  padding: 2em;
`;
export const TabContainer = styled(Box)`
  width: 345px;
  background: white;
  webkit-box-shadow: -1px 0px 5px 0px rgba(184, 184, 184, 0.5);
  -moz-box-shadow: -1px 0px 5px 0px rgba(184, 184, 184, 0.5);
  box-shadow: -1px 0px 5px 0px rgba(184, 184, 184, 0.5);
`;

export const Tab = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 117px;
  height: 40px;
  cursor: pointer;
  background: white;
  border-radius: 30px;
  color: #cacabf;
  font-family: "Airbnb Cereal App";
  font-size: 15px;
  font-weight: 500;
  ${({ active }) =>
    active &&
    `
    color: #81a957 ; 
    background: #e7efdf;
    font-weight: 700;
  `}
`;

export const TabGroup = styled(Flex)`
  justify-content: space-between;
  padding: 5px;
`;
