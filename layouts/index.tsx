import * as React from "react";
import Link from "next/link";
import { Nav, PageBody } from "./styles";

const Layout = ({ children }) => {
  return (
    <React.Fragment>
      <Nav>
        <Link href="/">
          <a>Homepage</a>
        </Link>
        <Link href="/page-two">
          <a>About</a>
        </Link>
      </Nav>
      <PageBody>{children}</PageBody>
    </React.Fragment>
  );
};

export default Layout;
