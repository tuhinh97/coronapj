export interface Item {
  id: number;
  title: string;
  imageUrl: string;
  description: string;
  numberDiscussions: number;
}

export interface coronaData {
  id?: number;
  title?: string;
  items?: Item[];
}
