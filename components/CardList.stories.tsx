import * as React from "react";
import CardList from "./CardList";
import { storiesOf } from "@storybook/react";
import { withKnobs, object } from "@storybook/addon-knobs";

storiesOf("Card", module)
  .addDecorator(withKnobs)

  .add("Default", () => {
    const label = "Styles";
    const defaultValue = [
      {
        id: 1,
        title: "Wear a facemask",
        imageUrl:
          "https://healthjournalism.org/blog/wp-content/uploads/2020/04/facemask-instructions-01-300x300.png",
        description:
          "You should wear facemask when you are around other people",
        numberDiscussions: 153,
      },
    ];
    const value = object(label, defaultValue);
    return <CardList items={value}></CardList>;
  });
