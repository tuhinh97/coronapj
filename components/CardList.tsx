import * as React from "react";
import { Box, Card, Image, Heading, Text, Flex } from "rebass";
import { TabContainer } from "../layouts/styles";
import { coronaData } from "./interfaces";
import Link from "next/link";

const CardList: React.FC<coronaData> = ({ items }: coronaData) => {
  return (
    <div>
      {items.map(({ id, title, imageUrl, description, numberDiscussions }) => {
        return (
          <TabContainer
            mb="19px"
            height="120px"
            key={id}
            sx={{
              borderRadius: "20px",
            }}
          >
            <Flex width="25em">
              <Box>
                <Card
                  sx={{
                    borderRadius: "30px",
                  }}
                >
                  <Image
                    src={imageUrl}
                    sx={{
                      margin: "10px",
                      width: "100px",
                      borderRadius: 12,
                    }}
                  />
                </Card>
              </Box>
              <Box width={[1, 1 / 2, 1 / 2]} mt="18px" ml="5px">
                <Heading
                  as="h3"
                  fontSize={16}
                  fontWeight="700"
                  fontFamily="Airbnb Cereal App"
                  mb="5px"
                >
                  {title}
                </Heading>
                <Text
                  color="#202E2E"
                  fontSize={13}
                  fontWeight="500"
                  fontFamily="Airbnb Cereal App"
                >
                  {description}
                </Text>
                <Link href="/discussions">
                  <a style={{ textDecoration: "none" }}>
                    <Text
                      mt="10px"
                      color="#7DA751"
                      fontSize={15}
                      fontWeight="500"
                      fontFamily="Airbnb Cereal App"
                    >
                      {numberDiscussions} Discussions →
                    </Text>
                  </a>
                </Link>
              </Box>
            </Flex>
          </TabContainer>
        );
      })}
    </div>
  );
};

export default CardList;
