const CORONA_DATA = [
  {
    id: 1,
    title: "Prevention",
    items: [
      {
        id: 1,
        title: "Wear a facemask",
        imageUrl:
          "https://healthjournalism.org/blog/wp-content/uploads/2020/04/facemask-instructions-01-300x300.png",
        description:
          "You should wear facemask when you are around other people",
        numberDiscussions: 153,
      },
      {
        id: 2,
        title: "Avoid close contact",
        imageUrl:
          "https://health.hawaii.gov/coronavirusdisease2019/wp-content/blogs.dir/116/files/2020/03/illustr-keeping_distance.jpg",
        description: "Put distance between yourself and other people",
        numberDiscussions: 127,
      },
      {
        id: 3,
        title: "Stay home if you're sick",
        imageUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQittu7ibK31UBm-jqpp1_Wgt-6wLUqWVwVFg&usqp=CAU",
        description: "Stay home if you are sick, except to get medical care.",
        numberDiscussions: 78,
      },
    ],
  },
  { id: 2, title: "Symptoms", items: [] },
  { id: 3, title: "Diagnosis", items: [] },
];
export default CORONA_DATA;
