import * as React from "react";
import TabsPreview from "./TabsPreview";

export default {
  component: TabsPreview,
  title: "TabsPreview",
};

const Template = () => <TabsPreview />;

export const Default = Template.bind({});
