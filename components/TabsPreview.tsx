import React, { useState } from "react";
import CORONA_DATA from "./corona.data";
import CardList from "./CardList";
import { TabContainer, Tab, TabGroup } from "../layouts/styles";

const types = CORONA_DATA;
const TabsPreview = () => {
  const [active, setActive] = useState(types[0]);
  return (
    <div>
      <TabContainer
        mb="10px"
        sx={{
          borderRadius: "30px",
          height: "50px",
        }}
      >
        <TabGroup>
          {types.map((type) => (
            <Tab
              key={type.id}
              active={active === type}
              onClick={() => setActive(type)}
            >
              {type.title}
            </Tab>
          ))}
        </TabGroup>
      </TabContainer>
      <div>
        <CardList items={active.items} />
      </div>
    </div>
  );
};

export default TabsPreview;
