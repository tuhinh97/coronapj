export const parameters = {
  backgrounds: {
    default: "default",
    values: [
      {
        name: "default",
        value: "#f4f0eb",
      },
      {
        name: "tomato",
        value: "#ed3d3d",
      },
    ],
  },
};
