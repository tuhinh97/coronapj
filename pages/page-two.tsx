import * as React from "react";
import Layout from "../layouts";

const PageTwo = () => {
  return <Layout>Some information</Layout>;
};

export default PageTwo;
