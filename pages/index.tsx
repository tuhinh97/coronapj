import * as React from "react";
import Layout from "../layouts";
import TabsPreview from "../components/TabsPreview";

const PageOne = () => {
  return (
    <Layout>
      <TabsPreview />
    </Layout>
  );
};
export default PageOne;
